//
//  ViewController.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 23.05.21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var citiesCollectionView: CityCollectionView!
    @IBOutlet weak var activityIndicatorView: UIView!
    
    private let weatherManager = WeatherManager()
    private let locationManager = LocationManager()
    
    private var currentCity: City?
    
    private var cities: [City]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestUserLocation()
    }
    
    private func updateUI() {
        
        if let lats = UserDefaults.standard.value(forKey: "Lat") as? [Double],
           let lons = UserDefaults.standard.value(forKey: "Lon") as? [Double],
           lats.count == lons.count {
            
            self.cities = nil
            
            var coords = [Coord]()
            for (index, lat) in lats.enumerated() {
                let coord = Coord(lat: lat, lon: lons[index])
                coords.append(coord)
            }
            
            
            if let currentCity = self.currentCity {
                if self.cities != nil {
                    cities?.insert(currentCity, at: 0)
                } else {
                    self.cities = [currentCity]
                }
            }
            
            for coord in coords {
                weatherManager.getCurrentWeather(lat: "\(coord.lat ?? 99.99)", lon: "\(coord.lon ?? 99.99)") { currentWeather in
                    let city = City(currentWeather: currentWeather)
                    
                    if self.cities != nil {
                        self.cities?.append(city)
                    } else {
                        self.cities = [city]
                    }
                    
                    if let cities = self.cities {
                        DispatchQueue.main.async {
                            self.citiesCollectionView.setup(cities: cities)
                        }
                    }
                } onFailure: {
                    print("fail")
                }
                
            }
            
        } else {
            
            self.cities = nil
            
            if let currentCity = self.currentCity {
                self.citiesCollectionView.setup(cities: [currentCity])
            }
        }
    }
    
    @IBAction private func addPlaceButtonClicked(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let addCityViewController = storyBoard.instantiateViewController(withIdentifier: "AddPlaceViewControllerIdentifier") as? AddPlaceViewController {
            addCityViewController.delegate = self
            present(addCityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction private func removeCityButtonClicked(_ sender: UIButton) {
        
        let currentIndex = Int(citiesCollectionView.contentOffset.x / citiesCollectionView.bounds.size.width)
        
        guard let count = self.cities?.count,
              currentIndex >= 0,
              currentIndex < count else {
            
            return
        }
        
        self.cities?.remove(at: currentIndex)
        if let cities = self.cities {
            self.citiesCollectionView.setup(cities: cities)
        }
    }
    
    private func save(city: City) {
        
        if let savedLat = UserDefaults.standard.value(forKey: "Lat") as? [Double], let savedLon = UserDefaults.standard.value(forKey: "Lon") as? [Double] {
            
            var newLat = savedLat
            if let cityLat = city.currentWeather?.coord?.lat {
                newLat.append(cityLat)
            }
            
            var newLon = savedLon
            if let cityLon = city.currentWeather?.coord?.lon {
                newLon.append(cityLon)
            }
            UserDefaults.standard.setValue(newLat, forKey: "Lat")
            UserDefaults.standard.setValue(newLon, forKey: "Lon")
        } else {
            if let cityLat = city.currentWeather?.coord?.lat {
                UserDefaults.standard.setValue([cityLat], forKey: "Lat")
            }
            
            if let cityLon = city.currentWeather?.coord?.lon {
                UserDefaults.standard.setValue([cityLon], forKey: "Lon")
            }
        }
    
        updateUI()
    }
}

extension ViewController: LocationManagerDelegate {
    
    func locationDidUpdate(lat: Double, lon: Double) {
        weatherManager.getCurrentWeather(lat: "\(lat)", lon: "\(lon)") { currentWeather in
            let city = City(currentWeather: currentWeather)
            
            self.currentCity = city
            
            DispatchQueue.main.async {
                self.updateUI()
                self.activityIndicatorView.isHidden = true
            }
        } onFailure: {
            print("fail")
        }
    }
}

extension ViewController: AddPlaceViewControllerDelegate {
    func placeDidSelect(place: Place) {
        if let lat = place.coord?.lat, let lon = place.coord?.lon {
            weatherManager.getCurrentWeather(lat: "\(lat)", lon: "\(lon)") { currentWeather in
                let city = City(currentWeather: currentWeather)
                
                self.save(city: city)
                
            } onFailure: {
                print("fail")
            }
        }
    }
}


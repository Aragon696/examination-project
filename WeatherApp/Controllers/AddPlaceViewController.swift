//
//  AddPlaceViewController.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import UIKit

protocol AddPlaceViewControllerDelegate: class {
    func placeDidSelect(place: Place)
}

class AddPlaceViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var placesTableView: PlacesTableView!
    
    weak var delegate: AddPlaceViewControllerDelegate?
    
    private let placeManager = PlaceManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        placesTableView.selectDelegate = self
        
        searchBar.becomeFirstResponder()
    }
    
    @IBAction private func closeButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddPlaceViewController: PlacesTableViewDelegate {
    
    func placeDidSelect(place: Place) {
        delegate?.placeDidSelect(place: place)
        dismiss(animated: true, completion: nil)
    }
}

extension AddPlaceViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let text = searchBar.text {
            placeManager.get(query: text) { places in
                if let placesList = places.list {
                    DispatchQueue.main.async {
                        self.placesTableView.setup(places: placesList)
                    }
                }
            } onFailure: {
                print("Fail")
            }
        }
    }
}

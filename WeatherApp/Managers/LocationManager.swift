//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 01.06.2021.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: class {
    func locationDidUpdate(lat: Double, lon: Double)
}

class LocationManager: NSObject {
    
    weak var delegate: LocationManagerDelegate?
    
    private var locationManager: CLLocationManager?
    
    override init() {
        super.init()
        
        configure()
    }
    
    private func configure() {
        locationManager = CLLocationManager()
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.delegate = self
    }
    
    func requestUserLocation() {
        locationManager?.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            
            delegate?.locationDidUpdate(lat: latitude, lon: longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // Handle failure to get a user’s location
    }
}

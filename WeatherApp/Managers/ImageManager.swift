//
//  ImageManager.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import UIKit

class ImageManager {
    
    func getImage(by name: String, onSuccess success: @escaping (_ image: UIImage) -> Void, onFailure fail: @escaping () -> Void) {
        
        if let url = URL(string: "http://openweathermap.org/img/wn/\(name)@2x.png") {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else {
                    fail()
                    return
                }
                
                if let image = UIImage(data: data) {
                    success(image)
                } else {
                    fail()
                }
            }
            
            task.resume()
        }
    }
}

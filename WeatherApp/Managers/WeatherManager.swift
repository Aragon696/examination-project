//
//  WeatherManager.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 23.05.21.
//

import Foundation

class WeatherManager {

    private let API_KEY = "02f2eac8265c5b11195d386b9290c8ae"

    func getCurrentWeather(lat: String, lon: String, onSuccess success: @escaping (_ currentWeather: CurrentWeather) -> Void, onFailure fail: @escaping () -> Void) {
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(API_KEY)") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { data, response, error in
            
            if error != nil {
                fail()
            } else if let data = data {
                
                let currentWeather: CurrentWeather = try! JSONDecoder().decode(CurrentWeather.self, from: data)
                
                success(currentWeather)
            } else {
                fail()
            }
        }
        
        task.resume()
    }
}

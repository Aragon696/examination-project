//
//  PlaceManager.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import Foundation

class PlaceManager {
//    https://openweathermap.org/data/2.5/find?q=London&sort=population&appid=439d4b804bc8187953eb36d2a8c26a02
    let API_KEY = "02f2eac8265c5b11195d386b9290c8ae"

    func get(query: String, onSuccess success: @escaping (_ places: Places) -> Void, onFailure fail: @escaping () -> Void) {
        
        guard let url = URL(string: "https://openweathermap.org/data/2.5/find?q=\(query)&sort=population&appid=439d4b804bc8187953eb36d2a8c26a02") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { data, response, error in
            
            if error != nil {
                fail()
            } else if let data = data {
                
                let places: Places = try! JSONDecoder().decode(Places.self, from: data)
                success(places)
            } else {
                fail()
            }
        }
        
        task.resume()
    }
}

//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 01.06.2021.
//

import Foundation

struct CurrentWeather: Codable {
    let name: String?
    let coord: Coord?
    let weather: [Weather]?
    let main: Main?
}

struct Weather: Codable {
    let id: Int?
    let main: String?
    let description: String?
    let icon: String?
}

struct Main: Codable {
    let temp: Double?
    let feels_like: Double?
    let temp_min: Double?
    let temp_max: Double?
    let pressure: Double?
    let humidity: Double?
    let sea_level: Double?
    let grnd_level: Double?
}

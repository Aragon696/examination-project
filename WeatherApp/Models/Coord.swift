//
//  Coord.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import Foundation

struct Coord: Codable {
    let lat: Double?
    let lon: Double?
}

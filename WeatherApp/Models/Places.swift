//
//  Places.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import Foundation

struct Places: Codable {
    let list: [Place]?
}

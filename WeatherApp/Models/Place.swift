//
//  Place.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import Foundation

struct Place: Codable {
    
    let id: Int?
    let name: String?
    let coord: Coord?
    let sys: Sys?
}

struct Sys: Codable {
    let country: String?
}

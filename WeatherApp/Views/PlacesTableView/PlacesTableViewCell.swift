//
//  PlacesTableViewCell.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import UIKit

class PlacesTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(place: Place) {
        nameLabel.text = place.name
        countryLabel.text = place.sys?.country
    }
    
}

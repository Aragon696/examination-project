//
//  PlacesTableView.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 6/6/21.
//

import UIKit

protocol PlacesTableViewDelegate: class {
    func placeDidSelect(place: Place)
}

class PlacesTableView: UITableView {
    
    weak var selectDelegate: PlacesTableViewDelegate?
    
    private var places = [Place]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        configure()
    }
    
    func setup(places: [Place]) {
        
        self.places = places
        reloadData()
    }
    
    private func configure() {
        delegate = self
        dataSource = self
        
        register(UINib(nibName: "PlacesTableViewCell", bundle: nil), forCellReuseIdentifier: "PlacesTableViewCellIdentifier")
    }
}

extension PlacesTableView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesTableViewCellIdentifier") as? PlacesTableViewCell else {
            return UITableViewCell()
        }
        
        cell.setup(place: places[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectDelegate?.placeDidSelect(place: places[indexPath.row])
    }
}

//
//  CityCollectionViewCell.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 03.06.2021.
//

import UIKit

class CityCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(city: City) {
        
        cityLabel.text = city.currentWeather?.name
        weatherDescriptionLabel.text = city.currentWeather?.weather?.first?.description
        
        if let tempK = city.currentWeather?.main?.temp {
            let tempC = tempK - 273.15
            let roundedTempC = Int(round(tempC))
            tempLabel.text = "\(roundedTempC)°"
        }
        
        if let minTempK = city.currentWeather?.main?.temp_min {
            let minTempC = minTempK - 273.15
            let roundedMinTempC = Int(round(minTempC))
            minTempLabel.text = "\(roundedMinTempC)°"
        }
        
        if let maxTempK = city.currentWeather?.main?.temp_max {
            let maxTempC = maxTempK - 273.15
            let roundedMaxTempC = Int(round(maxTempC))
            maxTempLabel.text = "\(roundedMaxTempC)°"
        }
        
        
        if let feelsLikeTempK = city.currentWeather?.main?.feels_like {
            let feelsLikeTempC = feelsLikeTempK - 273.15
            let roundedFeelsLikeTempC = Int(round(feelsLikeTempC))
            feelsLikeLabel.text = "feels like \(roundedFeelsLikeTempC)°"
        }
        
        if let humidity = city.currentWeather?.main?.humidity {
            humidityLabel.text = "\(humidity)%"
        }
        
        if let pressure = city.currentWeather?.main?.pressure {
            pressureLabel.text = "\(pressure)"
        }
        
        if let iconName = city.currentWeather?.weather?.first?.icon {
            
            updateBackgroundColor(iconName)
            
            ImageManager().getImage(by: iconName) { image in
                DispatchQueue.main.async {
                    self.iconImageView.image = image
                }
            } onFailure: {
                print("fail")
            }

        }
    }
    
    private func updateBackgroundColor(_ iconName: String) {
        switch iconName {
        case "01d":
            backgroundColor = UIColor(red: 119.0 / 255.0, green: 109.0 / 255.0, blue: 90.0 / 255.0, alpha: 1.0)
        case "02d":
            backgroundColor = UIColor(red: 152.0 / 255.0, green: 125.0 / 255.0, blue: 124 / 255.0, alpha: 1.0)
        case "03d":
            backgroundColor = UIColor(red: 160.0 / 255.0, green: 156.0 / 255.0, blue: 176.0 / 255.0, alpha: 1.0)
        case "04d":
            backgroundColor = UIColor(red: 163.0 / 255.0, green: 185.0 / 255.0, blue: 201.0 / 255.0, alpha: 1.0)
        case "09d":
            backgroundColor = UIColor(red: 171.0 / 255.0, green: 218.0 / 255.0, blue: 225.0 / 255.0, alpha: 1.0)
        case "10d":
            backgroundColor = UIColor(red: 120.0 / 255.0, green: 224.0 / 255.0, blue: 220.0 / 255.0, alpha: 1.0)
        case "11d":
            backgroundColor = UIColor(red: 142.0 / 255.0, green: 237.0 / 255.0, blue: 247.0 / 255.0, alpha: 1.0)
        case "13d":
            backgroundColor = UIColor(red: 35.0 / 255.0, green: 87.0 / 255.0, blue: 137.0 / 255.0, alpha: 1.0)
        case "50d":
            backgroundColor = UIColor(red: 241.0 / 255.0, green: 211.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0)
        case "01n":
            backgroundColor = UIColor(red: 142.0 / 255.0, green: 85.0 / 255.0, blue: 114.0 / 255.0, alpha: 1.0)
        case "02n":
            backgroundColor = UIColor(red: 242.0 / 255.0, green: 247.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
        case "03n":
            backgroundColor = UIColor(red: 194.0 / 255.0, green: 185.0 / 255.0, blue: 127.0 / 255.0, alpha: 1.0)
        case "04n":
            backgroundColor = UIColor(red: 188.0 / 255.0, green: 170.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
        case "09n":
            backgroundColor = UIColor(red: 136.0 / 255.0, green: 102.0 / 255.0, blue: 93.0 / 255.0, alpha: 1.0)
        case "10n":
            backgroundColor = UIColor(red: 54.0 / 255.0, green: 130.0 / 255.0, blue: 127.0 / 255.0, alpha: 1.0)
        case "11n":
            backgroundColor = UIColor(red: 55.0 / 255.0, green: 80.0 / 255.0, blue: 92.0 / 255.0, alpha: 1.0)
        case "13n":
            backgroundColor = UIColor(red: 17.0 / 255.0, green: 53.0 / 255.0, blue: 55.0 / 255.0, alpha: 1.0)
        case "50n":
            backgroundColor = UIColor(red: 63.0 / 255.0, green: 97.0 / 255.0, blue: 45.0 / 255.0, alpha: 1.0)
        default:
            break
        }
    }
}

//
//  CityCollectionView.swift
//  WeatherApp
//
//  Created by Yauheni Zagorski on 03.06.2021.
//

import UIKit

class CityCollectionView: UICollectionView {
    
    private var cities = [City]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        configure()
    }
    
    func setup(cities: [City]) {
        
        self.cities = cities
        reloadData()
    }
    
    private func configure() {
        
        delegate = self
        dataSource = self

        register(UINib(nibName: "CityCollectionViewCell",
                       bundle: nil), forCellWithReuseIdentifier: "CityCollectionViewCellIdentifier")
    }
}

extension CityCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cities.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: bounds.size.width, height: bounds.size.height)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCollectionViewCellIdentifier",
                                                            for: indexPath) as? CityCollectionViewCell else {
            return UICollectionViewCell()
        }

        cell.setup(city: cities[indexPath.row])

        return cell
    }
}
